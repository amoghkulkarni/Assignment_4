﻿window.onload = function () {
    
    var messages = [];
    var myusername = "";
    var histLength = -1;
    
    var socket = io.connect(window.location.href); // Where do we get this 'io' handle from??
    // Buttons
    var sendbutton = document.getElementById("sendbutton");
    var joinbutton = document.getElementById("joinbutton");
    var historybutton = document.getElementById("historybutton");
    // Output
    var chatbox = document.getElementById("chatbox");
    // Inputs
    var message = document.getElementById("message");
    var username = document.getElementById("username");
    var group = document.getElementById("group");
    var history = document.getElementById("history");
    // Drop-downs
    var mylist_incoming = document.getElementById("mylist_incoming");
    var mylist_outgoing = document.getElementById("mylist_outgoing");
    var mylist_groups = document.getElementById("mylist_groups");
    var mylist_users = document.getElementById("mylist_users");
    
    socket.on('message', function (data) {
//        alert('received message..');
        if (data.message) {
            if ((data.username != myusername) && (data.username != "@Server")) { // If someone else has sent me a message
                // play sound here for incoming message
                switch (mylist_incoming.options[mylist_incoming.selectedIndex].value) {
                    case 'Droplet':
                        var sound = new Audio('../sounds/Droplet.mp3');
                        sound.play();
                        break;
                    case 'Bells':
                        var sound = new Audio('../sounds/Bells.mp3');
                        sound.play();
                        break;
                    case 'Pizzicato':
                        var sound = new Audio('../sounds/Pizzicato.mp3');
                        sound.play();
                        break;
                    case 'Springtime':
                        var sound = new Audio('../sounds/Springtime.mp3');
                        sound.play();
                        break;
                    case 'Areyoukidding':
                        var sound = new Audio('../sounds/Areyoukidding.mp3');
                        sound.play();
                        break;
                    case 'Takethis':
                        var sound = new Audio('../sounds/Takethis.mp3');
                        sound.play();
                        break;
                    default:
                        break;
                }
            }
            
            if (data.username == "@Server") {
                if (data.message == 'welcomeMsg') {
                    data.username = "";
                    data.groupname = "";
                    data.ownername = "";
                    data.message = "Welcome to the chat!";
                }
                else if (data.message == "userExists") {
                    alert("Username already exists in this group. Please select another username.");
                }
                else if (data.message == "groupExists") {
                    alert("Groupname already exists. Please select another groupname.");
                    group.readOnly = false;
                    group.value = "";
                }
                else if (data.message.indexOf("permissionNewUser") != -1) {
                    var arrTemp = data.message.split(":");
                    var res = confirm(arrTemp[1] + " asked for your permission to join " + arrTemp[2] + ". Do you grant permssion?");
                    if (res == true) {
                        // Send response to the server saying you pressed ok - use a different "type" of message for this
                        socket.emit('permNewUserRes', { response: "YES", username: arrTemp[1] , groupname: arrTemp[2] , socketid: arrTemp[3] });
                    } else {
                        // Same as above
                        socket.emit('permNewUserRes', { response: "NO", username: arrTemp[1] , groupname: arrTemp[2] , socketid: arrTemp[3] });
                    }
                }
                else if (data.message.indexOf("permNewUserRes") != -1) {
                    var arrTemp = data.message.split(":");
                    if (arrTemp[1] == "YES") {
                        //modifying to introduce password
                        var pwd = prompt("The owner of the group " + arrTemp[2] + " has approved your request to join the group. Now please enter password");
                        socket.emit('joinReqPwd', { username: username.value, groupname: mylist_groups.options[mylist_groups.selectedIndex].value, socketid: socket.id, password: pwd });

                    }
                    else if (arrTemp[1] == "NO") {
                        alert("The owner of the group " + arrTemp[2] + " has declined your request to join the group.");
                        group.readOnly = false;
                        group.value = "";
                    }
                }
                else if (data.message.indexOf("pwdError") != -1) {
                    alert("The password that you entered was incorrect for this group. You cannot join this group. You can try again");
                    group.readOnly = false;
                    group.value = ""
                    mylist_groups.selectedIndex = 0;
                }
                else if (data.message.indexOf("pwdMatch") != -1) {
                    alert("You are now a part of this group");
                    group.readonly = true;
                }
                else if (data.message.indexOf("evicted") != -1) {
                    alert("You have been evicted from this group by the owner. You can now join other groups")
                    group.readOnly = false;
                    group.value = ""
                    mylist_groups.selectedIndex = 0;
                }
            }
            
            if ((data.groupname == group.value) || (data.username == "")) {
                messages.push(data);
                var html = '';
                if ((histLength > 0) && (histLength < messages.length)) { // When only 'some' of the message are to be shown
                    for (var i = messages.length - histLength; i < messages.length; i++) {
                        html += '<b>' + (messages[i].username ? messages[i].username : 'Server') + '@' + (messages[i].groupname ? messages[i].groupname : 'Server') + '@' + (messages[i].ownername ? messages[i].ownername : 'Server') + ': </b>';
                        html += messages[i].message + '<br />';
                    }
                }
                else {
                    for (var i = 0; i < messages.length; i++) { // When 'all' the messages are to be shown
                        html += '<b>' + (messages[i].username ? messages[i].username : 'Server') + '@' + (messages[i].groupname ? messages[i].groupname : 'Server') + '@' + (messages[i].ownername ? messages[i].ownername : 'Server') + ': </b>';
                        html += messages[i].message + '<br />';
                    }
                }
                chatbox.innerHTML = html;
            }
        } 
        else
            console.log("Received message is not well-formatted:", data);
    });
    
    historybutton.onclick = function () {
        var isHistoryNum = /^\d+$/.test(history.value);
        if (isHistoryNum) { // User has entered a number in history box
            histLength = parseInt(history.value);
        } else { // User hasn't entered a number in history box
            if (history.value == "All" || history.value == "all" || history.value == "ALL") {
                alert("History set to \"All\".");
                histLength = -1;
            } else {
                alert("Invalid history value! History set to default value of 10.");
                histLength = 10;
            }
        }
    }
    
    joinbutton.onclick = function () {
        if (username.value == "") {
            alert("Please type your username!");
        }
        else if (/[^a-zA-Z0-9]/.test(username.value)) {
            alert("Username cannot contain special characters!");
        }
        else {
            if (mylist_groups.options[mylist_groups.selectedIndex].value == "Default") {
                if (group.value == "") {
                    alert("Please type your group name!");
                }
                else if (/[^a-zA-Z\s0-9]/.test(group.value)) {
                    alert("Group name cannot contain special characters!");
                } else {
                    var pwd = prompt("Please enter your password for this group. All members would need to enter this password");
                    //alert(pwd)
                    socket.emit('createReq', { username: username.value, groupname: group.value, socketid: socket.id, password: pwd });
                    group.readOnly = true;
                }
            }
            else {
                //var pwd = prompt("Please enter the group password to join this group");
                socket.emit('joinReq', { username: username.value, groupname: mylist_groups.options[mylist_groups.selectedIndex].value, socketid: socket.id});
            }
        }
    }
    
    
    sendbutton.onclick = function () {
        var str = tinyMCE.get('message').getContent()
        if (username.value == "") {
            alert("Please type your username!");
        }
        else if (/[^a-zA-Z0-9]/.test(username.value)) {
            alert("Username cannot contain special characters!");
        }
        else if (str.length > 200) {
            alert("Message is too long");
        }
        else if (str == "") {
            alert("Please type your message!");
        }
        else {
            // First set username for this chat client            
            myusername = username.value;
            
            // play sound here for an outgoing message
            switch (mylist_outgoing.options[mylist_outgoing.selectedIndex].value) {
                case 'Droplet':
                    var sound = new Audio('../sounds/Droplet.mp3');
                    sound.play();
                    break;
                case 'Bells':
                    var sound = new Audio('../sounds/Bells.mp3');
                    sound.play();
                    break;
                case 'Pizzicato':
                    var sound = new Audio('../sounds/Pizzicato.mp3');
                    sound.play();
                    break;
                case 'Springtime':
                    var sound = new Audio('../sounds/Springtime.mp3');
                    sound.play();
                    break;
                case 'Areyoukidding':
                    var sound = new Audio('../sounds/Areyoukidding.mp3');
                    sound.play();
                    break;
                case 'Takethis':
                    var sound = new Audio('Takethis.mp3');
                    sound.play();
                    break;
                default:
                    break;
            }
            socket.emit('send', { message: str, username: username.value, groupname: group.value, ownername: "none" , recipient: mylist_users.value });
            tinyMCE.get('message').setContent('');
        }
    }
    
    
    mylist_groups.onclick = function () {
        group.readOnly = false;
        var xmlhttp;
        if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else { // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            var mylist_groups_index_buf = mylist_groups.selectedIndex;
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                
                // Parse the JSON string received in the response
                var groupNames = JSON.parse(xmlhttp.responseText);
                // Create options for the dropdown
                var html = "<option value=\"Default\">Create New Group</option>";
                for (var i = 0; i < groupNames.length ; i++) {
                    html += "<option value=\"" + groupNames[i] + "\"" + ">" + groupNames[i] + "</option>";
                }
                // Set the inner HTML
                mylist_groups.innerHTML = html;
            }
            mylist_groups.selectedIndex = mylist_groups_index_buf;
        }
        xmlhttp.open("GET", "/list_populate/groups", true);
        xmlhttp.send();
        if (mylist_groups.value != "Default") {
            joinbutton.innerHTML = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-log-in\"></span>  Join";
            group.value = mylist_groups.value;
            group.readOnly = true;
        }
        else {
            joinbutton.innerHTML = "<span aria-hidden=\"true\" class=\"glyphicon glyphicon-log-in\"></span>  Create";
            group.readOnly = false;
        }
    }
    
    
    mylist_users.onclick = function () {
        //Ayan: changing this and reading this from the groups Text Box. 
        //For assignment 4: evict needs to be done by owner. This was perhaps a bug with assignment 3
        //if (mylist_groups.value != "Default") { // For this AJAX call to happen, you should be in a group
            // i.e. Your mylist_groups value should be some group's name
        if (group.readOnly) { //Ayan: This is enough to check group
              //alert("inside")
            var xmlhttp;
            if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                var mylist_users_index_buf = mylist_users.selectedIndex;
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    
                    // Parse the JSON string received in response
                    var usernames = JSON.parse(xmlhttp.responseText);
                    // Create options for the dropdown
                    var html = "<option value=\"Default\">All</option>";
                    for (var i = 0; i < usernames.length ; i++) {
                        html += "<option value=\"" + usernames[i] + "\"" + ">" + usernames[i] + "</option>";
                    }
                    // Set the inner HTML
                    mylist_users.innerHTML = html;
                }
                mylist_users.selectedIndex = mylist_users_index_buf;
            }
            //xmlhttp.open("GET", "/list_populate/users?" + mylist_groups.value, true);
            xmlhttp.open("GET", "/list_populate/users?" + group.value, true);
            xmlhttp.send();
        }
    }
    evictbutton.onclick = function () {
        userToEvict = mylist_users.options[mylist_users.selectedIndex].value
        //alert(userToEvict)
        //if this is default, cannot be evicted
        if (userToEvict == "Default") {
            alert("Please select who needs to be evicted")
        }
        else if (userToEvict == username.value) {
            alert("You cannot evict yourself. Please press leave if you want to leave chat")
        }
        else {
            socket.emit('evictReq', { username: username.value, groupname: group.value, socketid: socket.id, usertoevict: userToEvict });
        }
    }
}
