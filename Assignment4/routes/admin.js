﻿var express = require('express');
var router = express.Router();

/* GET the home page of chat application. */
router.get('/', function (req, res) {
    res.render('admin');
});

module.exports = router;