﻿var groups = require('../model/database.js');

module.exports = function (server) {
    var io = require('socket.io').listen(server);
    console.log('listening..');
    
    io.sockets.on('connection', function (socket) {
        dataDict = { message: 'welcomeMsg', username: "@Server", groupname: "@Server", ownername: "@Server" };
        socket.emit('message', dataDict);
        
        socket.on('send', function (data) {
            for (var i = 0; i < groups.length; i++) {
                if (groups[i].name == data.groupname) {
                    data.ownername = groups[i].owner;
                }
            }
        
            if (data.recipient == "Default") { // If it is a message for everyone in the group
                io.sockets.emit('message', data);
            }

            else { // else if it is a private message
                for (var i = 0; i < groups.length ; i++) {
                    if (groups[i].name == data.groupname) {
                        // Send to the recipient
                        var recIndex = groups[i].members.indexOf(data.recipient);
                        var recSocket = groups[i].sockets[recIndex];
                        io.sockets.connected[recSocket].emit('message', data);
                    
                        // Also send the same message to the sender
                        var senderIndex = groups[i].members.indexOf(data.username);
                        var senderSocket = groups[i].sockets[senderIndex];
                        io.sockets.connected[senderSocket].emit('message', data);
                    }
                }
            }
        });

    
        socket.on('createReq', function (data) {
            var group = { name: data.groupname, owner: data.username, members: [data.username], sockets: [data.socketid], password: data.password};
            var i;
            for (i = 0 ; i < groups.length ; i++) {
                if (groups[i].name == group.name) {
                    // groupname already exists, tell user to change the groupname at the client side 
                    io.sockets.connected[data.socketid].emit("message", { message: "groupExists", username: "@Server", groupname: "@Server", ownername: "@Server" });
                    console.log("Create req failure: Group - " + data.groupname + " User - " + data.username);
                    break;
                }
            }
            if (i == groups.length) {
                groups.push(group);
                console.log("Create req success : Group - " + data.groupname + " User - " + data.username);
            }
        });
    
        socket.on('joinReq', function (data) {
            var i;
            for (i = 0; i < groups.length ; i++) {
                if (groups[i].name == data.groupname) {
                    if (groups[i].members.indexOf(data.username) == -1) { // if the username of the user doesn't already exists in the group
                        // check if password matches or not
                        //if (groups[i].password != data.password) {
                        //    messageTemp = "pwdError"
                        //    io.sockets.connected[data.socketid].emit('message', { message: messageTemp, username: "@Server", groupname: "@Server", ownername: "@Server" });
                        //}
                        //else {
                            // ask for permission from group owner
                            var ownerIndex = groups[i].members.indexOf(groups[i].owner);    // Take out the owner index -- isn't it 0 in each case??
                            var socketIdToBroadcast = groups[i].sockets[ownerIndex];        // Take out the socket ID of owner
                            messageTemp = 'permissionNewUser' + ':' + data.username + ':' + data.groupname + ':' + data.socketid;
                            io.sockets.connected[socketIdToBroadcast].emit('message', { message: messageTemp, username: "@Server", groupname: "@Server", ownername: "@Server" });
                        //}
                    }
                    else { // Tell user to change the username as it already exists in the group
                        console.log("Join req : Group - " + data.groupname + "already has User - " + data.username);
                        io.sockets.connected[data.socketid].emit('message', { message: "userExists" , username: "@Server" , groupname: "@Server" , ownername: "@Server" });
                    }
                }
            }
        });
    
        socket.on('permNewUserRes', function (data) { // The server callback on owner's response
            if (data.response == "YES") { // if data contains YES 
                // tell user in "username" that it has joined the group
                messageTemp = "permNewUserRes" + ":" + "YES" + ":" + data.groupname;
                io.sockets.connected[data.socketid].emit('message', { message: messageTemp , username: '@Server' , groupname: "@Server" , ownername : "@Server" });
                //for (var i = 0; i < groups.length ; i++) {
                //    if (groups[i].name == data.groupname) {
                //        groups[i].members.push(data.username);
                //        groups[i].sockets.push(data.socketid);
                //        console.log("Join req approved : Group - " + data.groupname + " User - " + data.username);
                //    }
                //}
            } else if (data.response == "NO") { // if data contains NO
                // tell user in "username" that it is not allowed to join the group
                messageTemp = "permNewUserRes" + ":" + "NO" + ":" + data.groupname;
                io.sockets.connected[data.socketid].emit('message', { message: messageTemp , username: '@Server' , groupname: "@Server" , ownername : "@Server" });
                console.log("Join req declined : Group - " + data.groupname + " User - " + data.username);
            } else {
                console.log("Response from owner for permission could not be handled.");
            }
        });
        
        socket.on('joinReqPwd', function (data) {
            var i;
            for (i = 0; i < groups.length ; i++) {
                if (groups[i].name == data.groupname) {
                    // check if password matches or not
                    if (groups[i].password != data.password) {
                        messageTemp = "pwdError"
                        io.sockets.connected[data.socketid].emit('message', { message: messageTemp, username: "@Server", groupname: "@Server", ownername: "@Server" });
                    }
                    else { //put user into the group
                        for (var i = 0; i < groups.length ; i++) {
                            if (groups[i].name == data.groupname) {
                                groups[i].members.push(data.username);
                                groups[i].sockets.push(data.socketid);
                                console.log("Join req approved : Group - " + data.groupname + " User - " + data.username);
                                messageTemp = 'pwdMatch' + ':' + data.username + ':' + data.groupname + ':' + data.socketid;
                                io.sockets.connected[data.socketid].emit('message', { message: messageTemp, username: "@Server", groupname: "@Server", ownername: "@Server" });
                            }
                        }

                    
                    }
                }
            }
            
        });

        socket.on('evictReq', function (data) { 
            //check if the person who has sent the request is the owner or not
            for (var i = 0; i < groups.length ; i++) {
                if (groups[i].name == data.groupname) {
                    if (groups[i].owner != data.username) {//this user has no authority to evict
                        messageTemp = "noAuthorityToEvict"
                        io.sockets.connected[data.socketid].emit('message', { message: messageTemp , username: '@Server' , groupname: "@Server" , ownername : "@Server" });
                        break;
                    }
                    else {//this is the owner, he can evict 
                        var recIndex = groups[i].members.indexOf(data.usertoevict);
                        var recSocket = groups[i].sockets[recIndex];//keep socket before deleting in case message needs to sent
                        groups[i].members.splice(recIndex, 1)//delete username
                        groups[i].sockets.splice(recIndex, 1)//delete socket 
                        messageTemp = "evicted"
                        io.sockets.connected[recSocket].emit('message', { message: messageTemp , username: '@Server' , groupname: "@Server" , ownername : "@Server" });     
                        break;
                    }
                }
            }
        });

    });
}
