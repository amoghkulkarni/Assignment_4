﻿var express = require('express');
var router = express.Router();

var groups = require('../model/database');

/* GET groups dropdown. */
router.get('/groups', function (req, res) {
    var groupNames = [];
    
    // Access 'groups' using the javascript in public directory,
    // which will provide access to the database
    for (var i = 0; i < groups.length ; i++)
        groupNames.push(groups[i].name);

    res.json(groupNames);
});

/* GET users dropdown. */
router.get('/users', function (req, res) {
    var group = req.url.substring(req.url.indexOf("?") + 1);
    var resStr = "";
    
    // Access 'groups' using the javascript in public directory,
    // which will provide access to the database
    for (var i = 0; i < groups.length ; i++) {
        if (groups[i].name == group) {
            resStr = groups[i].members;
            break;
        }
    }

    res.json(resStr);
});

module.exports = router;